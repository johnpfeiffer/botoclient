import argparse
# sudo pip install --upgrade boto  # dependency requirement, or use the source https://github.com/boto/boto
# http://boto.readthedocs.org/en/latest/s3_tut.html
# http://boto.readthedocs.org/en/latest/ref/s3.html#module-boto.s3.connection

import os
import sys
import time
import logging
import tempfile

from boto.s3.connection import S3Connection
from boto.s3.connection import S3ResponseError
from boto.s3.connection import ProtocolIndependentOrdinaryCallingFormat
from boto.s3.connection import OrdinaryCallingFormat
from boto.s3.key import Key

from boto.dynamodb2 import connect_to_region
from boto.dynamodb2.fields import HashKey, RangeKey, KeysOnlyIndex, AllIndex
from boto.dynamodb2.table import Table
from boto.dynamodb2.types import NUMBER, STRING

from boto.glacier.layer2 import Layer2  # http://boto.readthedocs.org/en/latest/ref/glacier.html
from boto.glacier.layer1 import Layer1  # http://boto.readthedocs.org/en/latest/ref/glacier.html
from boto.glacier.vault import Vault


class BotoClient(object):
    # TODO: deal with large lists

    def __init__(self, host, port, accessid, secret):
        self.accessid = accessid
        self.secret = secret
        self.is_ssl = True
        self.host = host
        self.port = int(port)
        # calling_format = ProtocolIndependentOrdinaryCallingFormat() # ProtocolIndependent requires host be http://fqdn
        self.calling_format = OrdinaryCallingFormat()
        self.layer1 = None
        self.layer2 = None
        self.vaults = None
        self.s3connect = None
        self.region = 'us-east-1'
        self.dynamoconnect = None

    def connect_s3(self):
        self.s3connect = S3Connection(self.accessid, self.secret, is_secure=self.is_ssl, port=self.port, host=self.host,
                                      calling_format=self.calling_format)
        if not self.s3connect:
            raise AssertionError('Unable to get a connection')

    def create_bucket(self, bucket_name):
        self.s3connect.create_bucket(bucket_name)

    def display_buckets(self):
        for b in self.get_buckets():
            print b.name

    def get_buckets(self):
        buckets = list()
        for b in self.s3connect.get_all_buckets():
            buckets.append(b)
        return buckets

    def get_bucket(self, name):
        for b in self.get_buckets():
            if name == b.name:
                return b

    def get_bucket_contents(self, name, prefix=None):
        contents = list()
        b = self.get_bucket(name)
        if b:
            for i in b.list(prefix=prefix):
                contents.append(i)
        return contents

    def download_listing(self, bucket, key_names, target_dir):
        for name in key_names:
            name_parts = name.split('/')
            if name_parts[-1]:
                local_file_path = os.path.join(target_dir, name_parts[-1])
                print local_file_path
                logging.info('downloading: {}'.format(local_file_path))
                with open(local_file_path, 'w') as f:
                    key = bucket.get_key(name)
                    key.get_contents_to_file(f)

    # TODO: put md5?
    def put_file(self, file_path, bucket, target_path):
        message = 'uploading {} to {}/{}'.format(file_path, bucket, target_path)
        logging.info(message)
        print message
        k = Key(bucket)
        k.key = target_path
        k.set_contents_from_filename(file_path)

    # DYNAMODB WORK IN PROGRESS

    def connect_dynamo(self):
        self.dynamoconnect = connect_to_region(self.region, aws_access_key_id=self.accessid,
                                                              aws_secret_access_key=self.secret)

    def get_dynamo_tables(self):
        tables_dict = self.dynamoconnect.list_tables()
        return tables_dict.get('TableNames')

    def create_dynamo_table(self, name, schema, throughput=None, indexes=None, connection=None):
        if connection is None:
            connection = self.dynamoconnect
        if throughput is None:
            throughput = {'read': 1, 'write': 1}
        if indexes is None:
            indexes = [AllIndex('EverythingIndex', parts=schema)]
        return Table.create(name, schema, throughput, indexes, connection)

    # GLACIER WORK IN PROGRESS

    def load_glacier_layers(self):
        self.layer2 = Layer2(aws_access_key_id=self.accessid, aws_secret_access_key=self.secret)
        self.layer1 = Layer1(aws_access_key_id=self.accessid, aws_secret_access_key=self.secret)

    def update_vault_listing(self):
        self.vaults = self.layer2.list_vaults()

    def display_vaults(self, detailed=False, list_jobs=False):
        for v in self.vaults:
            if detailed:
                print '{}, bytes: {}, {}, created: {}, last_inventory: {}'.\
                    format(v.name.encode('utf-8'), v.size, v.arn, v.creation_date, v.last_inventory_date)
            else:
                print v.name

            if list_jobs:
                jobs = v.list_jobs(completed=False)
                for j in jobs:
                    print j.id, j.action, j.creation_date, j.archive_id, j.status_message


        # vault = connection.get_vault(v)  # vault must already have been created

        # get the archiveId for the file, usually you pass the filename and file-like-object together
        # archiveID = vault.create_archive_from_file(fileName)

        # fileName = "LocalFileName"

    def initiate_inventory(self, vault_name):
        # self.connection.initiate_job(account_id='-', )
        job_id = self.layer1.initiate_job(vault_name,
                                          {"Description": "inventory-job", "Type": "inventory-retrieval",
                                           "Format": "JSON"})


# TODO: verify the putfile first http://stackoverflow.com/questions/11540854/file-as-command-line-argument-for-argparse-error-message-if-argument-is-not-va
def validate_args(args):
    puttarget_help_message = 'argument --puttarget must start with / followed by a bucket and then a key, e.g. /a/b'
    try:
        if args.putdirectory:
            if not args.puttarget:
                raise ValueError('--putdirectory requires a --puttarget')
            if not os.path.exists(args.putdirectory):
                raise ValueError('cannot find: --putdirectory {} '.format(args.putdirectory))
            if not os.path.isdir(args.putdirectory):
                raise ValueError('--putdirectory {} must be a directory'.format(args.putdirectory))
        if args.putfile:
            if not args.puttarget:
                raise ValueError('--putfile requires a --puttarget')
            if not os.path.exists(args.putfile):
                raise ValueError('cannot find: --putfile {} '.format(args.putfile))
            if not os.path.exists(args.putfile):
                raise ValueError('--putfile {} must be a file'.format(args.putfile))
        if args.puttarget:
            if not args.puttarget.startswith('/'):
                raise ValueError('{} is incorrect, {}'.format(args.puttarget, puttarget_help_message))
            if len(args.puttarget) < 4:
                raise ValueError('{} is incorrect, {}'.format(args.puttarget, puttarget_help_message))
        if args.downloaddir:
            if not args.listbucket:
                raise ValueError('--downloaddir requires a --listbucket, {}'.format(args.listbucket,
                                                                                    puttarget_help_message))

    except ValueError as arg_error:
        print 'ERROR: {}\n'.format(arg_error)
        parser.parse_args(['--help'])
        sys.exit(1)


if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('-a', '--accessid', help='Access Id (required)', required=True)
        parser.add_argument('-s', '--secret', help='Secret Key (required)', required=True)
        parser.add_argument('-H', '--host', help='Host', default='s3.amazonaws.com')
        parser.add_argument('-p', '--port', help='Port', default=443)
        parser.add_argument('--nossl', help='disable ssl validation', action='store_true')
        parser.add_argument('--newbucket', help='New bucket name (all lowercase)')
        parser.add_argument('--listbuckets', help='List bucket names', action='store_true')
        parser.add_argument('--listbucket', help='The name of a bucket to list the contents')
        parser.add_argument('--listprefix', help='key prefix (e.g. /pseudofolder/')
        parser.add_argument('--downloaddir', help='download files from listing to directory')

        parser.add_argument('--putfile', help='Filepath of the file to upload to a bucket, (requires a target)')
        parser.add_argument('--putdirectory', help='Filepath of the directory to upload to a bucket, (requires a target)')
        # type=argparse.FileType('r'))  # http://stackoverflow.com/questions/18862836/how-to-open-file-using-argparse
        parser.add_argument('--puttarget', help='Target path, /bucketname/pseudodir/objectkeyname, (requires a source)')

        parser.add_argument('--listtables', help='List DynamoDB Tables', action='store_true')
        parser.add_argument('--newtable', help='Name of a new DynamoDB Table')

        parser.add_argument('--glacier', help='Glacier related commands')
        parser.add_argument('--log', help='Specify the log file location')

        args = parser.parse_args()

        logging_output = args.log if args.log else os.path.join(tempfile.gettempdir(), sys.argv[0] + '.log')
        logging_level = logging.INFO
        logging.basicConfig(
            level=logging_level,
            format='%(asctime)s %(levelname)s %(message)s',
            filename=logging_output,
            filemode='a')

        validate_args(args)

        client = BotoClient(args.host, args.port, args.accessid, args.secret)

        if args.nossl:
            client.is_ssl = False
        # TODO: ssl invalid error handling

        if args.listbuckets or args.listbucket or args.puttarget or args.newbucket:

            logging.info('{} connecting to {} on port {} with ssl={}'.format(client.accessid, client.host, client.port,
                         client.is_ssl))
            client.connect_s3()
            logging.debug(repr(client.s3connect))

            if args.newbucket:
                client.create_bucket(args.newbucket)

            if args.listbuckets:
                client.display_buckets()

            if args.listbucket:
                listing = client.get_bucket_contents(args.listbucket, args.listprefix)
                if args.downloaddir:
                    bucket = client.get_bucket(args.listbucket)
                    key_list = list()
                    for i in listing:
                        key_list.append(i.name)
                    message = 'downloading {} items'.format(len(listing))
                    print message
                    logging.info(message)
                    client.download_listing(bucket, key_list, args.downloaddir)
                else:
                    for i in listing:
                        print '{} {} {} {}'.format(i.name.encode('utf-8'), i.size, i.md5, i.last_modified)

            if args.putfile and args.puttarget:
                target_path = args.puttarget.split('/')
                print target_path
                # get rid of the empty entry before the / that the split() creates
                target_path.pop(0)
                bucket_name = target_path.pop(0)

                # TODO: refactor to own method
                target_bucket = None
                for b in client.get_buckets():
                    if b.name == bucket_name:
                        target_bucket = b

                if target_bucket:
                    logging.info('uploading {} to {}'.format(args.putfile, target_bucket.name))
                    k = Key(target_bucket)
                    k.key = '/'.join(target_path)
                    k.set_contents_from_filename(args.putfile)
                else:
                    message = 'ERROR: target bucket {} from path {} does not exist'.format(bucket_name, args.puttarget)
                    print message
                    logging.error(message)

            # TODO: refactor code duplication
            if args.putdirectory and args.puttarget:
                target_path = args.puttarget.split('/')
                # get rid of the empty entry before the / that the split() creates
                target_path.pop(0)
                bucket_name = target_path.pop(0)
                logging.debug('bucket {}'.format(bucket_name))

                # TODO: refactor
                target_bucket = None
                for b in client.get_buckets():
                    if b.name == bucket_name:
                        target_bucket = b
                if target_bucket:
                    onlyfiles = [f for f in os.listdir(args.putdirectory) if os.path.isfile(os.path.join(args.putdirectory, f))]
                    print onlyfiles
                    logging.debug('putdirectory {}'.format(onlyfiles))
                    for f in onlyfiles:
                        print f
                        if target_path:
                            key_path = '/'.join(target_path) + '/' + f
                        else:
                            key_path = f
                        client.put_file(os.path.join(args.putdirectory, f), target_bucket, key_path)
                else:
                    message = 'ERROR: target bucket {} from path {} does not exist'.format(bucket_name, args.puttarget)
                    print message
                    logging.error(message)

        if args.listtables:
            client.connect_dynamo()
            for t in client.get_dynamo_tables():
                print t

        if args.newtable:
            client.connect_dynamo()
            # http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DataModel.html
            schema = [HashKey('server_id', data_type=STRING), RangeKey('email')]
            client.create_dynamo_table(args.newtable, schema)
            print 'created table {}'.format(args.newtable)

        # GLACIER WORK IN PROGRESS

        if args.glacier:
            client.load_glacier_layers()
            client.update_vault_listing()
            print client.display_vaults(detailed=True, list_jobs=True)
            # client.initiate_inventory('images')

    except KeyboardInterrupt:
        pass

    except S3ResponseError as error:
        print error
        logging.error(error)

    logging.info('done')
